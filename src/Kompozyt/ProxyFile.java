package Kompozyt;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProxyFile implements Node{
    private File file;

    public ProxyFile(File file){
        String pattern="[A-Za-z0-9/.]+";
        Pattern pattern1=Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
        Matcher matcher=pattern1.matcher(file.getName());
        boolean match=matcher.matches();
        if(match){
            this.file=file;
        }else{
            System.out.println("Invalid file name");
        }
    }

    @Override
    public void rename(String name) {
        String pattern="[A-Za-z0-9/.]+";
        Pattern pattern1=Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
        Matcher matcher=pattern1.matcher(name);
        boolean match=matcher.matches();
        if(match){
            file.rename(name);
        }else{
            System.out.println("Invalid file name");
        }

    }

    @Override
    public String getName() {
        return file.getName();
    }

    public String getContent() {
        return file.getContent();
    }

    public void setContent(String content) {
        file.setContent(content);
    }

    public void more(){
        file.more();
    }

}
