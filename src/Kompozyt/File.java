package Kompozyt;

public class File implements Node {
    private String content;
    private String name;

    @Override
    public void rename(String name) {
        this.name=name;
    }

    @Override
    public String getName() {
        return name;
    }

    public  File(String name){
        this.name=name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void more(){
        System.out.println(content);
    }
}
