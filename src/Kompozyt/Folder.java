package Kompozyt;

import java.util.ArrayList;

public class Folder implements Node {
    private final ArrayList<Node> elements = new ArrayList<>();
    private String name;

    @Override
    public void rename(String name) {
        this.name = name;
    }
    public Folder(String name){
        this.name=name;
    }

    @Override
    public String getName() {
        return name;
    }
    public void add(Node node) {
        elements.add(node);
    }

    public void remove(Node node) {
        elements.remove(node);
    }

    public Node getChild(String string){
        if(elements.isEmpty()) {
            System.out.println("Folder is empty");
            return null;
        }
        for (Node element : elements) {
            if (string.equals(element.getName()))
                return element;
        }
        System.out.println("Folder does not contain this element");
        return null;
    }

    public void ls(){
        for(Node node:elements){
            System.out.print(node.getName()+"\t");
        }
    }

    public void tree(){
        this.treeInternal(1);
    }

    public void treeInternal(int level) {
        System.out.println(getName());
        if(!elements.isEmpty()){
            for(int i=0;i<elements.size();i++){
                if(i==elements.size()-1){
                    if(level>1){
                        for(int j=0;j<level-1;j++){
                            System.out.print("\t");
                        }
                    }
                    System.out.print("└─\t");
                }else{
                    if(level>1){
                        for(int j=0;j<level-1;j++){
                            System.out.print("\t");
                        }
                    }
                    System.out.print("├─\t");
                }
                if(elements.get(i).getClass().getName().equals("Kompozyt.File")) {
                    System.out.println(elements.get(i).getName());
                }
                else{
                    ((Folder)elements.get(i)).treeInternal(level+1);
                }
            }
        }
    }
}